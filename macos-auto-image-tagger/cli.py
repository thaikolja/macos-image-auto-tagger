# Import the os module for interacting with the operating system
import os

# Import the sys module for system-specific parameters and functions
import sys

# Import the logging module to log error messages
import logging

# Import argparse to handle command-line arguments
import argparse

# Import dotenvfile to load environment variables from a .env file
import dotenvfile

# Import macos_tags to interact with macOS file tags
import macos_tags


class ImageTagger:
    """
    A class to handle image tagging using the Imagga API.
    """

    # Class-level constants for API credentials and file path
    API_KEY = ''
    API_SECRET = ''
    API_ENDPOINT = ''
    FILE_PATH = ''

    def __init__(self):
        """
        Initializes the ImageTagger instance by checking environment variables and command-line arguments.
        """
        self.__check_env()
        self.__check_args()

    def __check_env(self) -> bool:
        """
        Checks if the required environment variables are set. If not, logs an error and exits the program.

        Returns:
            bool: True if all required environment variables are set, otherwise exits the program.
        """
        # Load environment variables from the .env file
        env = dotenvfile.loadfile('./.env')

        # List of required environment variables
        variables = ['IMAGGA_API_KEY', 'IMAGGA_API_SECRET', 'IMAGGA_API_ENDPOINT']

        # Flag to track if any variable is unset
        unset = False

        for variable in variables:
            # Get the value from the environment or .env file
            value = env.get(variable) or os.getenv(variable)

            if not value:
                # Mark as unset if the variable is not found
                unset = True

                # Log an error message if the variable is not set
                logging.error(
                    f"Environment variable {variable} is not set. Read the README.md file for more information.")
            else:
                # Set the class attribute with the environment variable value
                setattr(self, variable.split('_', 1)[1], value)

        if unset:
            # Exit the program if any required variable is unset
            sys.exit(1)

        # Return True if all variables are set
        return True

    def __check_args(self) -> None:
        """
        Parses command-line arguments to get the image file path.
        """
        # Create an argument parser for command-line arguments
        parser = argparse.ArgumentParser(description='Adds tags to images using Imagga API')

        # Add a positional argument for the image file path
        parser.add_argument('image_path', help='Path to the image file')

        # Parse the command-line arguments
        args = parser.parse_args()

        # Assign the parsed image path to the class attribute
        file_path = args.image_path
        self.FILE_PATH = file_path

    def __check_file(self, file_path: str) -> bool:
        """
        Checks if the given file path exists and is a file. If not, logs an error and exits the program.

        Args:
            file_path (str): The path to the file to check.

        Returns:
            bool: True if the file exists and is a file, otherwise exits the program.
        """
        # Check if the file exists at the given path
        if not os.path.exists(file_path):
            # Log an error message if the file is not found
            logging.error(f"File {file_path} not found")
            sys.exit(1)

        # Check if the path is a file
        if not os.path.isfile(file_path):
            # Log an error message if the path is not a file
            logging.error(f"{file_path} is not a file")
            sys.exit(1)

        # Return True if the file exists and is a file
        return True

    def get_file_tags(self, file_path: str = '') -> list:
        """
        Retrieves tags for the given image file using the macOS tags.

        Args:
            file_path (str, optional): The path to the image file. Defaults to ''.

        Returns:
            list: A list of existing tags for the image file.
        """
        # Use the class attribute file path if no file path is provided
        if not file_path:
            file_path = self.FILE_PATH

        # Check if the file is valid
        self.__check_file(file_path)

        # Initialize a list to store existing tags
        existing_tags = []

        # Retrieve all tags from the file using macos_tags
        tags = macos_tags.get_all(file_path)

        # Return an empty list if no tags are found
        if not tags:
            return tags

        # Iterate over the tags and add their names to the list
        for tag in tags:
            if tag:
                existing_tags.append(tag.name)

        # Return the list of existing tag names
        return existing_tags

    def get_tags(self):
        """
        Retrieves tags for the given image file using the Imagga API.
        """
        pass


if __name__ == "__main__":
    # Create an instance of ImageTagger
    tagger = ImageTagger()

    # Get tags for the specified image
    tags = tagger.get_file_tags('img.jpg')
    print(tags)
