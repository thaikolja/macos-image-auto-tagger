# __init__.py for the macos_auto_image_tagger package

# Import necessary modules or classes to make them available at the package level
from .cli import ImageTagger
from .commands import *
from .utils import *

# Define what should be accessible when the package is imported
__all__ = ['cli', 'commands', 'utils']

# Package metadata
__version__ = '0.1.0'
__author__ = 'Kolja Nolte'

# You can also include any package-level initialization code here
